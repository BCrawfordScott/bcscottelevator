# Elevator System

High level: it needs to operate an arbitrary number of elevators across an arbitrary number of floors for an arbitrary number of users traveling in two different directions across any spread of those floors.

**Keep in mind**
- More than one person might get on at one floor
- People will get on and want to go in different directions
- Requests will emerge while elevators are in motion - we need to process them accordingly depending on the possible combinations of elevator states
- The elevator system should be able to interrupt any elevator with new requests
- the elevator system should keep track of which elevators are operating in which directions/idle
- Log elevator behavior

**Questions:**
- How do we establish a max number of passengers?
- Do we need a passenger object? (Can we have a placeholder for now?)
- Should we design service ranges for large numbers of floors? ex: 4 elevators for 200 floors: 1 - 50, 51 - 100, 101 - 150, 151 - 200
- Should we validate the floor requests?
- How do we properly order destinations on a route? 
- Should there be a pickups queue for an elevator as well as a destinations queue?
- How will the elevator system determine which elevator should pick up which passengers?
- Edge case: how do we serve passengers who board and elevator heading in the opposite direction of their destination?
- Should the elevators hold a reference to their system to broadcast state changes? Could we use a broadcast proc? 
- Edge case, should and elevator throw a warning if the floor request is in the opposite path of it's travel?


Use outline: 
- Baseline: all elevators are idle at their start positions (floor 1)
- 1 passenger calls an elevator on floor 1 with a request direction
- 1 elevator receives two passengers
- Each passenger's destination floor is registered by the elevator receiving the passengers.
- The elevator travels in one direction until it reaches the first destination
- the elevator reachesthe first destination in order and becomes idle
- the passenger(s) with that destination disembark(s)
- the elevator checks to see if it has more destinations in it's current direction; it continues through it's destination repeating the above steps or it attempts to return to it's base state.


An elevator 

## Objects:
- Elevator System (Singleton?)
    - Initialize with number of elevators and number of floors
    - Keep reference to all instances of elevator that are created
    - receive all floor requests
    - identify best elevator to serve request
    - Assign elevators to requests
    - ~~Keep track of all passengers waiting to be picked up?~~ _Passengers will essentially be assigned to elevators for pickup, and elevator will maintain it's own queue, and the number of passengers waiting can always be arbitrary._

- Elevator
    - Keep track of state/direction: idle, up, down
    - know if you're "returning to starting position" or "at starting position"
    - Keep track of number of passengers
    - keep track of queue of pickups
    - Keep track of queue of destinations
    - to start (at least) ~~only serve~~ prioritize serving passengers moving in one direction
- Passenger(?)
    - For now this can exist as a hash with:
        - current_floor
        - destination
        - request direction can be inferred from the relation of those two data points

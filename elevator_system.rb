require_relative './elevator.rb'

class ElevatorSystem
    def initialize(props)
        @floors = props[:floors]
        @idle = [] # represents elevators at their "starting point".     
        @up = [] # represents elevators at currently going up.     
        @down = [] # represents elevators currently going down.     
        @elevators = props[:elevators].times do 
            @idle << Elevator.new(self.method(:recieve_broadcast))
        end
    end

    def receive_request(passenger) # passenger is assumed to look like : {current_floor: 1, destination: 10}
        puts "Received request on floor #{passenger[:current_floor]}"
        choose_elevator(passenger[:current_floor], passenger[:destination])
    end

    def time_passed # explicit call for unit of time
        puts "Time passed"
        @elevators.each { |el| el.time_passed }
    end

    def recieve_broadcast(elevator)
        case elevator.direction
        when :idle
            idle.push(elevator) unless idle.include?(elevator)
        when :up
            up.push(elevator) unless up.include?(elevator)
        when :down
            down.push(elevator) unless down.include?(elevator)
        else 
            return 
        end
    end

    private

    attr_reader :floors, :elevators, :idle, :up, :down

    def choose_elevator(passenger) # identify the best elevator for a given request floor + destination
        dir = passenger[:current_floor] < passenger[:destination] ? :up : :down
        if idle.length > 0
            elevator = idle.shift
            up << elevator
            assign_request(passenger, elevator) 
        elsif dir == :up
            elevator = up.find { |el| el.location < req } || up.first
            assign_request(passenger, elevator)
        else
            elevator = down.find { |el| el.location > req } || down.first
            assign_request(passenger, elevator)
        end 
    end

    def assign_request(passenger, elevator) # add the request to the elevator's pickup queue
        elevator.receive_request(request)
    end

end

class Elevator

    attr_accessor :direction, :passengers
    attr_reader :request_queue
    attr_reader :destination_queue

    def initialize(broadcast, passenger_limit = 5, location = 1)
        @broadcast = broadcast # proc to signal to the system "I'm idle"
        @passenger_limit = pasenger_limit # start with 5 for default
        @direction = :idle
        @location = location # all elevators start on floor 1, aka lobby, aka 0, etc
        @destination_queue = [] # preferably a priority queue
        @request_queue = [] # preferably a priority queue
    end

    def time_passed
        self.direction = :idle if should_be_idle?

        unless idle
            unload_passengers
            pickup_passengers
            advance
        end


    end

    def receive_request(passenger)
        if self.direction == :idle
            self.direction = floor > location ? :up : :down
            broadcast_dir
        end

        request_queue.push(passenger).sort do |a, b| # sorting in lieu of pQueue implementation
            if direction == :down
                b[:current_floor] <=> a[:current_floor]
            else 
                a[:current_floor] <=> b[:current_floor] 
            end
        end
    end
    

    private

    attr_reader :broadcast

    # def returning_to_start?
    #     direction == :down && destinations.length == 0
    # end

    def reset
        self.direction = :idle
        broadcast.call(self)
    end

    def broadcast_dir
        broadcast.call(direction)
    end

    def receive_passengers(*passengers)
        # recieve any number of passengers
        # insert their destinations into the destinations_queue
        passengers.each do |passenger|
            destination_queue.push(passenger[:destination]).sort do |a, b| # sorting in lieu of pQueue implementation
                if direction == :down
                    b <=> a
                else 
                    a <=> b 
                end
            end
        end
    end

    def should_be_idle?
        direction != :idle && location == 1 && destination_queue.lenght == 0
    end

    def idle
        direction == :idle
    end

    def unload_passengers
        if location == destination_queue.first
            destination_queue.shift
            puts "Unloading passengers on floor #{location}"
        end
    end

    def pickup_passengers
        pass_request = request_queue.first
        if location == pass_request[:current_floor]
            request_queue.shift
            puts "Picking up passengers on floor #{location}"
            receive_passengers(request_queue.reject! { |pass| pass[:current_floor] == location })
        end
    end

    def advance
        return_to_lobby if empty
        location += 1 if direction == :up
        location -+ 1 if direction == :down
    end

    def empty?
        destination_queue.length == 0 && request_queue.lenght == 0
    end

    def return_to_lobby
        # logic to return to the lobby
    end
end
